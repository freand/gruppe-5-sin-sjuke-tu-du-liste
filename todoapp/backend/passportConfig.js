const bcrypt = require('bcryptjs');
const localStrategy = require('passport-local').Strategy;
const pool = require('./mysql-pool');

module.exports = function (passport) {
  passport.use(
    new localStrategy((username, password, done) => {
      pool.query('Select * FROM Users WHERE Username=?', [username], (error, results) => {
        if (error) return console.error(error);
        if (!results.length) return done(null, false);
        bcrypt.compare(password, results[0].Password, (err, result) => {
          if (err) return console.error(err);
          if (result === true) {
            return done(null, results[0]);
          } else {
            return done(null, false);
          }
        });
      });
    })
  );

  passport.serializeUser((user, cb) => {
    cb(null, user.UserID);
  });
  passport.deserializeUser((id, cb) => {
    pool.query('SELECT * FROM Users WHERE UserID=?', [id], (error, user) => {
      const userInformation = {
        ID: user[0].UserID,
        Username: user[0].Username,
      };
      cb(error, userInformation);
    });
  });
};
