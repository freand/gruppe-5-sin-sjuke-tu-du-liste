//import { pool } from 'mysql-pool';
const pool = require('./mysql-pool');
class GroupService {
  getGroups(success) {
    pool.query('SELECT * FROM Groups', (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  getGroup(id, success) {
    pool.query('SELECT * FROM Groups WHERE ID=?', [id], (error, results) => {
      if (error) return console.error(error);

      success(results[0]);
    });
  }

  updateGroup(group, success) {
    pool.query('UPDATE Groups SET Name=? WHERE ID=?', [group.Name, group.ID], (error, results) => {
      if (error) return console.error(error);

      success();
    });
  }
  addGroup(name, owner, success) {
    pool.query(
      'INSERT INTO Groups (Name, Owner) VALUES (?, ?)',
      [name, owner],
      (error, results) => {
        if (error) return console.error(error);
        success();
      }
    );
    
  }
  removeGroup(id, success) {
    pool.query('DELETE FROM Groups WHERE ID=?', [id], (error, results) => {
      if (error) return console.log(error);

      success();
    });
  }
}

class ListService {
  getLists(ParentGroup, success) {
    pool.query('SELECT * FROM Lists WHERE ParentGroup=?', [ParentGroup], (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  getList(ID, success) {
    pool.query('SELECT * FROM Lists WHERE ID=?', [ID], (error, results) => {
      if (error) return console.error(error);

      success(results[0]);
    });
  }

  updateList(list, success) {
    pool.query(
      'UPDATE Lists SET ListName=?, WHERE ID=?',
      [list.ListName, list.ListID],
      (error, results) => {
        if (error) return console.error(error);

        success();
      }
    );
  }

  removeList(id, success) {
    pool.query('DELETE FROM Lists WHERE ListID=?', [id], (error, results) => {
      if (error) return console.log(error);

      success();
    });
  }

  addList(list, success) {
    pool.query(
      'INSERT INTO Lists (ListName,ParentGroup) VALUES (?,?)',
      [list.ListName, list.ParentGroup],
      (error, results) => {
        if (error) return console.error(error);

        success(results.insertId);
      }
    );
  }
}

class TodoService {
  getTodos(ParentList, success) {
    pool.query(
      'SELECT *, IF(Checked, "true", "false") Checked FROM TodoItems WHERE ParentList=?',
      [ParentList],
      (error, results) => {
        if (error) return console.error(error);

        success(results);
      }
    );
  }

  getTodo(id, success) {
    pool.query('SELECT * FROM TodoItems WHERE TodoID=?', [id], (error, results) => {
      if (error) return console.error(error);

      success(results[0]);
    });
  }

  updateTodo(todo, success) {
    pool.query(
      'UPDATE TodoItems SET Title=?, Description=?, Priority=?, EndDate=? WHERE TodoID=?',
      [todo.Title, todo.Description, todo.Priority, todo.EndDate, todo.TodoID],
      (error, results) => {
        if (error) return console.error(error);
        success();
      }
    );
  }

  checkTodo(todo, success) {
    pool.query(
      'UPDATE TodoItems SET Checked = NOT Checked WHERE TodoID=?', //Flipper checked kolonnen.
      [todo],
      (error, results) => {
        if (error) return console.error(error);

        success();
      }
    );
  }

  addTodo(todo, ParentList, success) {
    pool.query(
      'INSERT INTO TodoItems (Title,Description,EndDate,ParentList) VALUES (?,?,?,?)',
      [todo.Name, todo.Description, todo.EndDate, ParentList],
      (error, results) => {
        if (error) return console.error(error);
        success();
      }
    );
  }
  removeTodo(id, success) {
    pool.query('DELETE FROM TodoItems WHERE TodoID=?', [id], (error, results) => {
      if (error) return console.log(error);

      success();
    });
  }
  TodoTitleSort(id, sort, success) {
    pool.query(
      'SELECT *, IF(Checked, "true", "false") Checked FROM TodoItems WHERE ParentList=? ORDER BY ?', //Det er her det blir satt inn i feil format?
      [id, sort],
      (error, results) => {
        if (error) return console.log(error);
        console.log(results);
        success(results);
      }
    )
  } 
}

class Loginservice {
  getUser(user, success) {
    pool.query(
      'SELECT * FROM Users WHERE Username=? AND Password=?',
      [user.username, user.password],
      (error, results) => {
        if (error) return console.error(error);

        success(results);
      }
    );
  }
}
// export let groupService = new GroupService();
// export let listService = new ListService();
// export let todoService = new TodoService();
// export let loginService = new Loginservice();
exports.groupService = new GroupService();