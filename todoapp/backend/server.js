const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors');
const passport = require('passport');
const passportlocal = require('passport-local').Strategy;
const cookieParser = require('cookie-parser');
const bcrypt = require('bcryptjs');
const session = require('express-session');
const app = express();

const pool = require('./mysql-pool');

//import  { groupService, listService, loginService, todoService } from './services';
const groupService = require('./services');
const { end } = require('./mysql-pool');
// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  cors({
    origin: 'http://localhost:3000',
    credentials: true,
  })
);

app.use(
  session({
    secret: 'secretcode',
    resave: true,
    saveUninitialized: true,
  })
);

app.use(cookieParser('secretcode'));
app.use(passport.initialize());
app.use(passport.session());
require('./passportConfig')(passport);

//Routes
app.post('/login', (req, res, next) => {
  passport.authenticate('local', (error, user, info) => {
    if (error) throw error;
    if (!user) res.send('User does not exit');
    else {
      req.logIn(user, (err) => {
        if (err) throw err;
      });
      res.send('login success');
    }
  })(req, res, next);
});
app.post('/register', (req, res) => {
  console.log(req.body);
  pool.query(
    'Select * FROM Users WHERE Username=?',
    [req.body.username],
    async (error, results) => {
      if (error) return console.error(error);
      if (results.length) res.send('Username taken');
      if (!results.length) {
        const hashedPassword = await bcrypt.hash(req.body.password, 10);
        pool.query(
          'INSERT INTO Users (Username, Password) VALUES (?, ?)',
          [req.body.username, hashedPassword],
          (error, results) => {
            if (error) return console.error(error);
          }
        );
        res.send('registration success');
      }
    }
  );
});

app.get('/user', (req, res) => {
  res.send(req.user);
});

app.get('/logout', (req, res) => {
  req.logout();
  res.send('logout success');
});
//Start server
app.listen(4000, () => {
  console.log('Server has started');
});

//SQL

app.get('/groups', (req, res) => {
  new dbRequest().getAllNoParent('Groups', (answer) => {
    res.send(answer);
  });
});

app.get('/groups/:groupId/lists/', (req, res) => {
  new dbRequest().getAllParent(
    'Lists',
    'ParentGroup',
    Number.parseInt(req.params.groupId),
    'ListID',
    (answer) => {
      res.send(answer);
    }
  );
});

app.get('/groups/:groupId/lists/:listId/todos/:sort?', (req, res) => {
  new dbRequest().getAllParent(
    'TodoItems',
    'ParentList',
    Number.parseInt(req.params.listId),
    req.params.sort,
    (answer) => {
      res.send(answer);
    }
  );
});

app.post('*/new/', (req, res) => {
  console.log(req.body.endDate);
  new dbRequest().newEntry(
    req.body.name,
    req.body.owner,
    req.body.table,
    req.body.endDate,
    (answer) => {
      res.send(answer);
    }
  );
});

app.post('/groups/del/', (req, res) => {
  console.log(req.body);
  new dbRequest().del(req.body.id, 'Groups', (answer) => {
    res.send(answer);
  });
});

app.post('*/lists/del/', (req, res) => {
  console.log(req.body);
  new dbRequest().del(req.body.id, 'Lists', (answer) => {
    res.send(answer);
  });
});

app.post('*/todos/del/', (req, res) => {
  console.log(req.body);
  new dbRequest().del(req.body.id, 'TodoItems', (answer) => {
    res.send(answer);
  });
});

app.post('*/change/', (req, res) => {
  console.log(req.body);
  new dbRequest().changeTodo(
    req.body.id,
    req.body.desc,
    req.body.priority,
    req.body.endDate,
    (answer) => {
      res.send(answer);
    }
  );
});

app.post('*/flipCheck', (req, res) => {
  console.log(req.body);
  new dbRequest().flipChecked(req.body.id, req.body.checked, (answer) => {
    res.send(answer);
  });
});
// app.get('/Groups/:groupId/Lists/:listId/Todos/:todoId', (req, res) => {
//   if (req.params.tootId) {
//     res.send(req.params.groupId);
//   } else if (req.params.todoId) {
//     res.send('heh');
//   }
// });
class dbRequest {
  getAllNoParent(table, successCallback) {
    let fromQuery = 'SELECT * FROM ' + table;
    pool.query(fromQuery, (error, results) => {
      if (error) return console.error(error);
      successCallback(results);
    });
  }

  getAllParent(table, parent, id, sort, successCallback) {
    if (table === 'Lists') {
      var fromQuery = 'SELECT * FROM ' + table + ' WHERE ' + parent + ' = ' + id;
    } else {
      var fromQuery =
        'SELECT TodoID, Title, Priority, Description, Checked, StartDate, DATE_FORMAT(EndDate,"%Y-%m-%d") as EndDate FROM ' +
        table +
        ' WHERE ' +
        parent +
        ' = ' +
        id +
        ' ORDER BY ' +
        sort;
    }
    pool.query(fromQuery, (error, results) => {
      if (error) return console.error(error);
      successCallback(results);
    });
  }

  newEntry(name, owner, table, endDate, successCallback) {
    if (table == 'Groups') {
      var entireQuery = 'INSERT INTO ' + table + ' (Name, Owner) VALUES (?, ?)';
    } else if (table == 'Lists') {
      var entireQuery = 'INSERT INTO ' + table + ' (ListName, ParentGroup) VALUES (?, ?)';
    } else {
      var entireQuery =
        'INSERT INTO ' + table + ' (Title, ParentList, endDate) VALUES (?, ?, "' + endDate + '")';
    }
    pool.query(entireQuery, [name, owner], (error, results) => {
      if (error) return console.error(error);
      successCallback(results);
    });
  }

  del(id, table, successCallback) {
    if (table == 'Groups') {
      var entireQuery = 'DELETE FROM ' + table + ' WHERE ID=?';
    } else if (table == 'Lists') {
      var entireQuery = 'DELETE FROM ' + table + ' WHERE ListID=?';
    } else {
      var entireQuery = 'DELETE FROM ' + table + ' WHERE TodoID=?';
    }
    pool.query(entireQuery, [id], (error, results) => {
      if (error) return console.error(error);
      successCallback(results);
    });
  }
  changeTodo(id, desc, priority, enddate, successCallback) {
    pool.query(
      'UPDATE TodoItems SET Description=?, Priority=?, EndDate=? WHERE TodoID=?',
      [desc, priority, enddate, id],
      (error, results) => {
        if (error) return console.error(error);
        successCallback(results);
      }
    );
  }
  flipChecked(id, checked, successCallback) {
    pool.query('UPDATE TodoItems SET Checked=? WHERE TodoID=?', [checked, id], (error, results) => {
      if (error) return console.error(error);
      successCallback(results);
    });
    console.log(id);
  }
}
