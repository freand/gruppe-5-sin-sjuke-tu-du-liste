import axios from 'axios';
import React from 'react';
import { Link, Router } from 'react-router-dom';

export default class Lists extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lists:[],
            loading: true,
            listName: ''
            
        };
        this.handleChange = this.handleChange.bind(this); //For å håndtere input
    }
    handleChange(event) { this.setState({listName: event.target.value}); } //For å håndtere input

    newQuery = async () => {
        try {
            const res = await axios.get('http://localhost:4000'+this.props.location.pathname);
            this.setState({
                lists: res.data,
                loading: false 
            });
            console.log(res);
        } catch (err) {
            console.error(err);
        }
    };
    componentDidMount() {
        this.newQuery();
        this.lookupInterval = setInterval(
            () => this.newQuery(),
            500
        );
    }
    componentWillUnmount() {
        clearInterval(this.lookupInterval);
    }

    addList = async () => {
        try {
            console.log(this.state.listName);
            const res = await axios.post('http://localhost:4000'+this.props.location.pathname+'/new', {
                name: this.state.listName,
                owner: this.props.match.params.id,
                table: 'Lists'
            });
            console.log(res);
        } catch (err) {
            console.error(err);
        }
    }

    removeList = async (selected) => {
        try {
            const updated = this.state.lists.filter(i => i.ListID !== selected.ListID)
            this.setState({
                lists: updated
            })
            const res = await axios.post('http://localhost:4000'+this.props.location.pathname+'/del', {
                id: selected.ListID
        });
        console.log(res);
        } catch (err) {
            console.error(err);
        }
    }

    render () {
        const { loading, lists } = this.state;
        if (loading && lists.length === 0) {
            return (
                <p> Loading... </p>
            )
        }
        return (
            <div>
                <h1>Your lists</h1>
                {this.state.lists ? <h1>  
                    {this.state.lists.map((list) => (
                        <div>
                        <li key={list.ListID}>
                        <Link to={window.location.pathname + '/' + list.ListID + '/todos/'}>{list.ListName}</Link>
                        </li>
                            <button onClick={this.removeList.bind(this, list)}>Delete</button>
                        </div>  
                    ))}
                </h1> : null}
                <label htmlFor="newListInput">Create new list:</label>
                    <input
                        id="newListInput"
                        type="text"
                        placeholder="List name"
                        value={this.state.value} //For å håndtere input
                        onChange={this.handleChange}
                    />
                    <button type="button" onClick={this.addList}>
                        Add
                    </button>
            </div>
        )
    }
}
