import React, { useState } from 'react';
import axios from 'axios';
import './App.css';

export function Login() {
  const [registerUsername, setRegisterUsername] = useState('');
  const [registerPassword, setRegisterPassword] = useState('');
  const [loginUsername, setLoginUsername] = useState('');
  const [loginPassword, setLoginPassword] = useState('');

  const register = () => {
    axios({
      method: 'post',
      data: {
        username: registerUsername,
        password: registerPassword,
      },
      withCredentials: true,
      url: 'http://localhost:4000/register',
    }).then((res) => {
      if (res.data === 'registration success') {
        console.log('Registration complete');
        window.location.href = '/groups';
      }
    });
  };

  const login = () => {
    axios({
      method: 'post',
      data: {
        username: loginUsername,
        password: loginPassword,
      },
      withCredentials: true,
      url: 'http://localhost:4000/login',
    }).then((res) => {
      if (res.data === 'login success') {
        //history.push("/groups");
        console.log('Logged in');
        window.location.href = '/groups';
      }
    });
  };

  return (
    <div className="App">
      <div>
        <label htmlFor="registerUsername">
          <h1>Register</h1>
        </label>
        <input
          id="registerUsername"
          placeholder="username"
          onChange={(e) => setRegisterUsername(e.target.value)}
        />
        <input
          type="password"
          id="registerpassword"
          placeholder="password"
          onChange={(e) => setRegisterPassword(e.target.value)}
        />
        <button onClick={register}>Register</button>
      </div>
      <div>
        <label htmlFor="loginUsername">
          <h1>Login</h1>
        </label>
        <input
          id="loginUsername"
          placeholder="username"
          onChange={(e) => setLoginUsername(e.target.value)}
        />
        <input
          type="password"
          id="loginPassword"
          placeholder="password"
          onChange={(e) => setLoginPassword(e.target.value)}
        />
        <button onClick={login}>Login</button>
      </div>
    </div>
  );
}
