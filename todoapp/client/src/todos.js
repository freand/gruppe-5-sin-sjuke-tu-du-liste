import axios from 'axios';
import React from 'react';
import Modal from 'react-modal';
import './index.css';

Modal.setAppElement('#root');

export default class TodoItems extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            todoItems:[],
            loading: true,
            todoName: '',
            showTodoModal: false,
            currentTodo: {},
            endDate: '',
            sort: 'StartDate',
            checked: false,
            };
        this.handleChange = this.handleChange.bind(this); //For å håndtere input
        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleDescChange = this.handleDescChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.handleSort = this.handleSort.bind(this);
        this.handlePrioChange = this.handlePrioChange.bind(this);
    }
    
    handleChange(event) { this.setState({todoName: event.target.value}); } //For å håndtere input
    handleChangeDate(event) { this.setState({endDate: event.target.value}); }

    handleOpenModal (selected) {
        const chosenTodo = this.state.todoItems.filter(i => i.TodoID === selected.TodoID)[0];
        this.setState({ showTodoModal: true, currentTodo: chosenTodo });
        console.log(chosenTodo)
    }
    handleCloseModal () {
        this.setState({ showTodoModal: false, currentTodo: {} });
    }
    handleDescChange(event) {
        let updatedCurrentTodo = this.state.currentTodo;
        updatedCurrentTodo.Description = event.target.value;
        this.setState({currentTodo: updatedCurrentTodo});
    }
    handleDateChange(event) {
        let updatedCurrentTodo = this.state.currentTodo;
        updatedCurrentTodo.EndDate = event.target.value;
        this.setState({currentTodo: updatedCurrentTodo});
    }
    handleSort(event) {
        this.setState({sort: event.target.value});
    }
    handleCheck = async (selected) => {
        const chosenTodo = this.state.todoItems.filter(i => i.TodoID === selected.TodoID)[0]
        chosenTodo.Checked = !chosenTodo.Checked;
        try {
            const res = await axios.post('http://localhost:4000'+this.props.location.pathname+'flipCheck', {
                id: chosenTodo.TodoID,
                checked: chosenTodo.Checked,
            });
        } catch (err) {
            console.error(err);
        }
    }
    handlePrioChange(event) {
        let updatedCurrentTodo = this.state.currentTodo;
        updatedCurrentTodo.Priority = event.target.value;
        this.setState({currentTodo: updatedCurrentTodo});
    }
    newQuery = async () => {
        try {
            const res = await axios.get('http://localhost:4000'+this.props.location.pathname+this.state.sort);
            this.setState({
                todoItems: res.data,
                loading: false 
            });
        } catch (err) {
            console.error(err);
        }
    };
    componentDidMount() {
        this.newQuery();
        this.lookupInterval = setInterval(
            () => this.newQuery(),
            500
        );
    }
    componentWillUnmount() {
        clearInterval(this.lookupInterval);
    }

    addTodo = async () => {
        try {
            console.log(this.props.match.params.listid);
            const res = await axios.post('http://localhost:4000'+this.props.location.pathname+'new', {
                name: this.state.todoName,
                endDate: this.state.endDate,
                owner: this.props.match.params.listid,
                table: 'TodoItems',
            });
            console.log(res);
        } catch (err) {
            console.error(err);
        }
    }

    removeTodo = async (selected) => {
        try {
            const updated = this.state.todoItems.filter(i => i.TodoID !== selected.TodoID)
            this.setState({
                todoItems: updated
            })
            const res = await axios.post('http://localhost:4000'+this.props.location.pathname+'del', {
                id: selected.TodoID
        });
        console.log(res);
        } catch (err) {
            console.error(err);
        }
    }

    saveChanges = async () => {
        try {
            const res = await axios.post('http://localhost:4000'+this.props.location.pathname+'change', {
                    id: this.state.currentTodo.TodoID,
                    desc: this.state.currentTodo.Description,
                    priority: this.state.currentTodo.Priority,
                    endDate: this.state.currentTodo.EndDate,
        });
        } catch (err) {
            console.error(err);
        }
    }
    renderPriority(prio) {
        switch(prio) {
            case 0:
                return 'Not important';
            case 1:
                return 'Slightly important';
            case 2:
                return 'Important';
            case 3:
                return 'Very important';
        }
    }
        
    render () {
        const { loading, todoItems } = this.state;
        if (loading && todoItems.length === 0) {
            return (
                <p> Loading... </p>
            )
        }
        return (
            <div id="root ">
                <h1>Your todoItems</h1>
                <form className="sort">
                <label htmlFor="sortBy">Sort: </label>
                    <select value={this.state.sort} onChange={this.handleSort} id ="sortBy">
                        <option value="Title">Title</option>
                        <option value="Priority">Priority</option>
                        <option value="StartDate">Start date</option>
                        <option value="EndDate">End date</option>
                    </select>
                    </form>
                {this.state.todoItems ? <div> 
                    {this.state.todoItems.map((todo) => (
                        <div className="listContainer">
                            {todo.Checked ? (
                            <input
                            type="checkbox"
                            id={'check' + todo.TodoID}
                            name={todo.TodoID}
                            onClick={this.handleCheck.bind(this, todo)}
                            defaultChecked
                            />
                        ) : (<input
                            type="checkbox"
                            id={'check' + todo.TodoID}
                            name={todo.TodoID}
                            onClick={this.handleCheck.bind(this, todo)}
                            />)}{' '}

                        <li onClick={this.handleOpenModal.bind(this, todo)} key={todo.TodoID} className="list-item">{todo.Title}</li>
                        <div className="todoInfoContainer">
                            <li className="todoInfo">{this.renderPriority(todo.Priority)}</li>
                            <li className="todoInfo">Enddate: {todo.EndDate}</li>
                        </div>
                            <button className="deleteButtonTodo" onClick={this.removeTodo.bind(this, todo)}>Delete</button>
                        </div>  
                    ))}
                </div> : null}
                <label htmlFor="newTodoInput">Create new todo:</label>
                    <input
                        id="newTodoInput"
                        type="text"
                        placeholder="Todo name"
                        value={this.state.value} //For å håndtere input
                        onChange={this.handleChange}
                    />
                    <label htmlFor="newTodoInputDate">EndDate:</label>
                    <input id="newTodoInputDate" type="date" onChange={this.handleChangeDate}/>
                    <button type="button" onClick={this.addTodo}>
                        Add
                    </button>
                    <Modal 
                        isOpen={this.state.showTodoModal}
                        contentLabel="Todo Modal View"
                    >
                     
                        <button onClick={this.handleCloseModal}>Close</button>
                        <h1>{this.state.currentTodo.Title}</h1>
                        <textarea style = {{border: 'none'}} value = {this.state.currentTodo.Description} onChange={this.handleDescChange}></textarea>
                        <p>Enddate: {this.state.currentTodo.EndDate}</p>
                        <input type="date" value={this.state.currentTodo.EndDate} onChange={this.handleDateChange}/>
                        <label htmlFor="sortBy">Priority: </label>
                            <select value={this.state.currentTodo.Priority} onChange={this.handlePrioChange} id ="priority">
                                <option value="0">Not important</option>
                                <option value="1">Slightly important</option>
                                <option value="2">Important</option>
                                <option value="3">Very important</option>
                            </select>
                        <button onClick={this.saveChanges}>Save changes</button>

                    </Modal>
            </div>

        )
    }
}