import axios from 'axios';
import React, { useContext } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { myContext } from '../context';
import { BrowserRouter } from 'react-router-dom';

export default function NavBar() {
    const ctx = useContext(myContext);
    const history = useHistory();

    const logout = () => {
        axios({ 
            withCredentials: true,
            url: 'http://localhost:4000/logout',
          }).then((res) => {
            if (res.data === "logout success") {
                //history.push("/");
                console.log("Logged out");
                window.location.href = "/";
            }
          });
    }
    return (
        <div className="NavContainer">
            {ctx ? (
                 <>
                <Link onClick={logout} to="/logout">Logout</Link>{" "}
                <Link to="/profile">Profile</Link>{" "}
                <Link to="/groups">Groups</Link>{" "}
                </>
            ) : (
                <>
            <Link to="/">Login/Register</Link>{" "}
            </>
            )
            }
        </div>
    )
} 