import axios from 'axios';
import React from 'react';
import { Link } from 'react-router-dom';
import { myContext } from './context';


class Groups extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            groups:[],
            loading: true,
            groupName: ''
        };
        this.handleChange = this.handleChange.bind(this); //For å håndtere input
    }

    handleChange(event) { this.setState({groupName: event.target.value}); } //For å håndtere input

    newQuery = async () => {
        try {
            const res = await axios.get('http://localhost:4000'+this.props.location.pathname);
            this.setState({
                groups: res.data,
                loading: false 
            });
            console.log(res);
        } catch (err) {
            console.error(err);
        }
    };
    componentDidMount() {
        this.newQuery();
        this.lookupInterval = setInterval(
            () => this.newQuery(),
            500
        );
    }
    componentWillUnmount() {
        clearInterval(this.lookupInterval);
    }

    addGroup = async () => {
        try {
            console.log(this.context);
            const res = await axios.post('http://localhost:4000'+this.props.location.pathname+'/new', {
                name: this.state.groupName,
                owner: this.context.ID,
                table: 'Groups'
            });
            console.log(res);
        } catch (err) {
            console.error(err);
        }
    }

    removeGroup = async (selected) => {
        try {
            const updated = this.state.groups.filter(i => i.ID !== selected.ID)
            this.setState({
                groups: updated
            })
            const res = await axios.post('http://localhost:4000'+this.props.location.pathname+'/del', {
                id: selected.ID
        });
        console.log(res);
        } catch (err) {
            console.error(err);
        }
    }
    render () {
        const { loading, groups } = this.state;
        if (loading && groups.length > 0) {
            return (
                <p> Loading... </p>
            )
        }
        return (
            <div>
                <h1>Your groups</h1>
                {this.state.groups ? <h1>  
                    {this.state.groups.map((group) => (
                        <>
                        <li key={group.ID}>
                        <Link to={'/groups/' + group.ID + '/lists'}>{group.Name}</Link>
                        </li>
                        <button onClick={this.removeGroup.bind(this, group)}>Delete</button>
             </>
                    ))}
                </h1> : null}

                    <label htmlFor="newGroupInput">Create new group:</label>
                    <input
                        id="newGroupInput"
                        type="text"
                        placeholder="Group name"
                        value={this.state.value} //For å håndtere input
                        onChange={this.handleChange}
                    />
                    <button type="button" onClick={this.addGroup}>
                        Add
                    </button>
            </div>
            
          
        )
    }
}
Groups.contextType = myContext;
export { Groups }; 