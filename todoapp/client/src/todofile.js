import react from 'react';
import axios from 'axios';
import React from 'react';
import { Link, Router } from 'react-router-dom';

export default class TodoItems extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            todoItems:[],
            loading: true,
            todoName: ''
        };
        this.handleChange = this.handleChange.bind(this); //For å håndtere input
    }
    handleChange(event) { this.setState({todoName: event.target.value}); } //For å håndtere input

    newQuery = async () => {
        this.setState({ loading: true });
        try {
            const res = await axios.get('http://localhost:4000'+this.props.location.pathname);
            this.setState({
                todoItems: res.data,
                loading: false 
            });
            console.log(res);
        } catch (err) {
            console.error(err);
        }
    };
    componentDidMount() {
        this.newQuery();
        this.lookupInterval = setInterval(
            () => this.newQuery(),
            10000
        );
    }
    componentWillUnmount() {
        clearInterval(this.lookupInterval);
    }

    addTodo = async () => {
        try {
            console.log(this.props.match.params.listid);
            const res = await axios.post('http://localhost:4000'+this.props.location.pathname+'/new', {
                name: this.state.todoName,
                owner: this.props.match.params.listid,
                table: 'TodoItems'
            });
            console.log(res);
        } catch (err) {
            console.error(err);
        }
    }

    removeTodo = async (selected) => {
        try {
            const updated = this.state.todoItems.filter(i => i.TodoID !== selected.TodoID)
            this.setState({
                todoItems: updated
            })
            const res = await axios.post('http://localhost:4000'+this.props.location.pathname+'/del', {
                id: selected.TodoID
        });
        console.log(res);
        } catch (err) {
            console.error(err);
        }
    }

    render () {
        const { loading, todoItems } = this.state;
        if (loading && todoItems.length === 0) {
            return (
                <p> Loading... </p>
            )
        }
        return (
            <div>
                <h1>Your todo</h1>
                {this.state.todoItems ? <h1>  
                    {this.state.todoItems.map((todo) => (
                        <div>
                            {JSON.parse(todo.Checked.toLowerCase()) && (
                            <input
                            type="checkbox"
                            id={'check' + todo.TodoID}
                            name={todo.TodoID}
                            onClick={this.handleCheck}
                            defaultChecked
                            />
                        )}{' '}
                        {/*Json parse to change the boolean string to boolean*/}
                        {!JSON.parse(todo.Checked.toLowerCase()) && (
                            <input
                            type="checkbox"
                            id={'check' + todo.TodoID}
                            name={todo.TodoID}
                            onClick={this.handleCheck}
                            />
                        )}{' '}
                        <li key={todo.TodoID}>
                        <Link to={window.location.pathname + '/todoItems/' + todo.TodoID}>{todo.Title}</Link>
                        </li>
                            <button onClick={this.removeTodo.bind(this, todo)}>Delete</button>
                        </div>  
                    ))}
                </h1> : null}
                <label htmlFor="newTodoInput">Create new todo:</label>
                    <input
                        id="newTodoInput"
                        type="text"
                        placeholder="Todo name"
                        value={this.state.value} //For å håndtere input
                        onChange={this.handleChange}
                    />
                    <button type="button" onClick={this.addTodo}>
                        Add
                    </button>
            </div>
        )
    }
}