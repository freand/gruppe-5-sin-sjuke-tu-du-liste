import React, { useContext } from 'react';
import { Route, Switch, BrowserRouter, Redirect } from 'react-router-dom';

import { myContext } from './context';

import { Groups } from './groups';
import GroupLists from './lists';
import ListTodos from './todos';
import {Login} from './login';
import NavBar from './Components/navbar';

import './index.css';

function App() {
    const ctx = useContext(myContext);
    console.log(ctx);
    return (
      <BrowserRouter>
          <NavBar />
          <Switch>
            {ctx ? (
              <>
                <Route exact path="/"><Redirect to="/groups"/></Route>
                <Route exact path="/groups" component={Groups} />
                <Route exact path="/groups/:id/lists" component={GroupLists} />
                <Route exact path="/groups/:id/lists/:listid/todos/" component={ListTodos} />
              </>
            ) : (
              <>
                <Route exact path="/" component={ctx ? <Redirect to="/groups" /> : Login} />
              </>
            )}
          </Switch>
      </BrowserRouter>
    );
  }
export default App;
//https://stackoverflow.com/questions/37669391/how-to-get-rid-of-underline-for-link-component-of-react-router
//https://reactrouter.com/web/api/history