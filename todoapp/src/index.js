import * as React from 'react';
import { Component } from 'react-simplified';
import ReactDOM from 'react-dom';
import { NavLink, HashRouter, Route } from 'react-router-dom';
import { groupService, listService, loginService, todoService } from './services';
import { createHashHistory } from 'history';

const history = createHashHistory(); // Use history.push(...) to programmatically change path, for instance after successfully saving a group

class Menu extends Component {
  render() {
    return (
      <div>
        <NavLink exact to="/" activeStyle={{ color: 'darkblue' }}>
          Logout
        </NavLink>
        {' ' /* Add extra space between menu items */}
        <NavLink to="/groups" activeStyle={{ color: 'darkblue' }}>
          Groups
        </NavLink>
      </div>
    );
  }
}

class Home extends Component {
  user = [];

  render() {
    return (
      <div>
        <p id="instructions">Please login to access your To-do items.</p>
        <form>
          {' '}
          {/*A simple login page. */}
          <input
            type="text"
            id="username"
            onChange={(event) => (this.user.username = event.currentTarget.value)}
          />
          <input
            type="password"
            id="password"
            onChange={(event) => (this.user.password = event.currentTarget.value)}
          />
          <button type="button" onClick={this.login}>
            Login
          </button>
        </form>
      </div>
    );
  }
  login() {
    if (!(Object.keys(this.user).length === 0)) {
      //Makes sure the username has been filled in.
      loginService.getUser(this.user, (answer) => {
        console.log(answer);
        if (typeof answer[0] === 'undefined') {
          document.getElementById('instructions').innerHTML += '</br>No match found';
        } else if (
          this.user.username == answer[0].Username &&
          this.user.password == answer[0].Password
        ) {
          history.push('/groups');
        }
      });
    }
  }
}

class GroupList extends Component {
  groups = [];

  render() {
    return (
      <div>
        <ul>
          {this.groups.map((group) => (
            <li key={group.ID}>
              <NavLink to={'/groups/' + group.ID + '/lists'}>{group.Name}</NavLink>
            </li>
          ))}
          <br></br>
        </ul>
        <div>
          Remove group:
          <select
            defaultValue={'def'}
            onChange={(event) => (this.groups.ID = event.currentTarget.value)}
          >
            <option hidden disabled value="def">
              Choose group
            </option>
            {this.groups.map((groups) => (
              <option key={groups.ID} value={groups.ID}>
                {groups.Name}
              </option>
            ))}
          </select>
          <button type="button" onClick={this.remove}>
            Remove
          </button>
        </div>
        <div>
          Create new group:
          <input
            type="text"
            placeholder="Group name"
            onChange={(event) => (this.groups.Name = event.currentTarget.value)}
          />
        </div>

        <button type="button" onClick={this.add}>
          Add
        </button>
      </div>
    );
  }

  mounted() {
    groupService.getGroups((groups) => {
      this.groups = groups;
    });
  }
  add() {
    groupService.addGroup(this.groups, () => {
      console.log('Created a new group');
      window.location.reload();
    });
  }
  remove() {
    groupService.removeGroup(this.groups.ID, () => {
      console.log('Group is removed');
      window.location.reload();
    });
  }
}

class GroupEdit extends Component {
  group = null;

  render() {
    if (!this.group) return null;

    return (
      <div>
        Name:{' '}
        <input
          type="text"
          value={this.group.Name}
          onChange={(event) => (this.group.Name = event.currentTarget.value)}
        />
        <button type="button" onClick={this.save}>
          Save
        </button>
      </div>
    );
  }

  mounted() {
    groupService.getGroup(this.props.match.params.id, (group) => {
      this.group = group;
    });
  }

  save() {
    groupService.updateGroup(this.group, () => {
      history.push('/groups');
    });
  }
}

class ListList extends Component {
  AllListes = [];
  NewList = { ListName: '', ParentGroup: this.props.match.params.id };

  render() {
    return (
      <div>
        <button type="button" onClick={() => history.goBack()}>
          Go back
        </button>
        <ul>
          {this.AllListes.map((list) => (
            <li key={list.ListID}>
              <NavLink to={'/Lists/' + list.ListID + '/todos'}>{list.ListName}</NavLink>
            </li>
          ))}
        </ul>
        <div>
          Remove list:
          <select
            defaultValue={'def'}
            onChange={(event) => (this.AllListes.ListID = event.currentTarget.value)}
          >
            <option hidden disabled value="def">
              Choose list
            </option>
            {this.AllListes.map((Lists) => (
              <option key={Lists.ListID} value={Lists.ListID}>
                {Lists.ListName}
              </option>
            ))}
          </select>
          <button type="button" onClick={this.remove}>
            Remove
          </button>
        </div>
        <br></br>
        <div>
          Create new list:
          <input
            type="text"
            placeholder="List name"
            onChange={(event) => (this.NewList.ListName = event.currentTarget.value)}
          />
        </div>

        <button type="button" onClick={this.add}>
          Add
        </button>
      </div>
    );
  }

  mounted() {
    listService.getLists(this.props.match.params.id, (lists) => {
      this.AllListes = lists;
    });
  }

  remove() {
    listService.removeList(this.AllListes.ListID, () => {
      console.log('List is removed from the group');
      window.location.reload();
    });
  }

  add() {
    listService.addList(this.NewList, () => {
      console.log('Added a new list to your group');
      window.location.reload(); //Refreshes site to see new list.
    });
  }
}

class TodoList extends Component {
  todo = null;
  todoItems = [];
  Priorities = [
    {
      prio: 'Normal',
      id: 0,
    },
    {
      prio: 'Urgent',
      id: 1,
    },
    {
      prio: 'Not important',
      id: 2,
    },
  ]; //needed for priorities
  TodoCollum = [
    {
      name: 'Title',
      key: 'Title',
    },
    {
      name: 'Priority',
      key: 'Priority',
    },
  ];

  render() {
    return (
      <div>
        <button type="button" onClick={() => history.goBack()}>
          Go back
        </button>
        <ul>
          {this.todoItems.map((todo) => (
            <li key={todo.TodoID}>
              {JSON.parse(todo.Checked.toLowerCase()) && (
                <input
                  type="checkbox"
                  id={'check' + todo.TodoID}
                  name={todo.TodoID}
                  onClick={this.save}
                  defaultChecked
                />
              )}{' '}
              {/*Json parse to change the boolean string to boolean*/}
              {!JSON.parse(todo.Checked.toLowerCase()) && (
                <input
                  type="checkbox"
                  id={'check' + todo.TodoID}
                  name={todo.TodoID}
                  onClick={this.save}
                />
              )}{' '}
              {/* If boolean is true, the checkbox is checked. The opposite is the case if false. */}
              <NavLink to={'/todos/' + todo.TodoID}>{todo.Title}</NavLink>{' '}
              {this.Priorities[todo.Priority].prio}
            </li>
          ))}
          <br></br>
        </ul>
        <div>
          Remove todo item:
          <select
            defaultValue={'def'}
            onChange={(event) => (this.todoItems.TodoID = event.currentTarget.value)}
          >
            <option hidden disabled value="def">
              Choose list
            </option>
            {this.todoItems.map((todoItems) => (
              <option key={todoItems.TodoID} value={todoItems.TodoID}>
                {todoItems.Title}
              </option>
            ))}
          </select>
          <button type="button" onClick={this.remove}>
            Remove
          </button>
        </div>
        <div>
          New todo item:{' '}
          <input
            type="text"
            placeholder="Todo item name"
            onChange={(event) => (this.todoItems.Name = event.currentTarget.value)}
          />{' '}
          <input
            type="text"
            placeholder="Todo description"
            onChange={(event) => (this.todoItems.Description = event.currentTarget.value)}
          />{' '}
          <input
            type="date"
            placeholder="End Date"
            onChange={(event) => (this.todoItems.EndDate = event.currentTarget.value)}
          />
          <button type="button" onClick={this.add}>
            Add
          </button>
        </div>
        <div>
          <select onChange={(event) => (this.TodoCollum.key = event.currentTarget.value)}>
            {this.TodoCollum.map((Sort) => (
              <option key={Sort.key} value={Sort.key}>
                {Sort.name}
              </option>
            ))}
          </select>
          <button type="button" onClick={this.sortTodo}>
            Sort this list
          </button>
        </div>
      </div>
    );
  }

  mounted() {
    todoService.getTodos(this.props.match.params.id, (todos) => {
      this.todoItems = todos;
    });
    console.log(this.TodoCollum);
  }

  save(todo) {
    todoService.checkTodo(todo.target.name, () => {});
  }
  add() {
    todoService.addTodo(this.todoItems, this.props.match.params.id, () => {
      console.log('Added a new todo-item to your list');
      window.location.reload();
    });
  }
  remove() {
    todoService.removeTodo(this.todoItems.TodoID, () => {
      console.log('Removed todo from list');
      window.location.reload();
    });
  }
  sortTodo() {
    let newKey = this.TodoCollum.key.replace(/'/g, '`'); //Forsøk på å bytte format
    console.log(newKey);
    todoService.TodoTitleSort(this.props.match.params.id, newKey, (Sorted) => {
      console.log(Sorted);
      this.todoItems = Sorted;
      console.log(this.TodoCollum.key);
    });
  }
  // ListSort() {
  //   todoService.SortTodoName(this.props.match.params.id, () => {
  //     console.log('Sorted by title')
  //     history.push('/todos');
  //   })
  // }
}

class ToDoListDetails extends Component {
  ItemDetail = null;
  Priorities = [
    {
      prio: 'Normal',
      id: 0,
    },
    {
      prio: 'Urgent',
      id: 1,
    },
    {
      prio: 'Not important',
      id: 2,
    },
  ];

  render() {
    if (!this.ItemDetail) return null;

    return (
      <div>
        <button type="button" onClick={() => history.goBack()}>
          Go back
        </button>{' '}
        <ul>
          {this.ItemDetail.Title}
          <li>
            {' '}
            Priotiry:
            {this.Priorities[this.ItemDetail.Priority].prio}
          </li>
          <li>
            {' '}
            Start date: {this.ItemDetail.StartDate.getDate()}
            {'.'}
            {this.ItemDetail.StartDate.getMonth() + 1}
            {'.'}
            {this.ItemDetail.StartDate.getFullYear()}
          </li>
          <li>
            {' '}
            End date: {this.ItemDetail.EndDate.getDate()}
            {'.'}
            {this.ItemDetail.EndDate.getMonth() + 1}
            {'.'}
            {this.ItemDetail.EndDate.getFullYear()}
          </li>
          <li>Description: {this.ItemDetail.Description}</li>
        </ul>
        <div>
          <input
            type="text"
            placeholder="Change todo item name"
            onChange={(event) => (this.ItemDetail.Title = event.currentTarget.value)}
          />{' '}
          <input
            type="text"
            placeholder="Change todo description"
            onChange={(event) => (this.ItemDetail.Description = event.currentTarget.value)}
          />{' '}
          <li>
            <select
              value={this.ItemDetail.Priority}
              onChange={(event) => (this.ItemDetail.Priority = event.currentTarget.value)}
            >
              {this.Priorities.map((priorities) => (
                <option key={priorities.id} value={priorities.id}>
                  {priorities.prio}
                </option>
              ))}
            </select>
          </li>
          {/* <input
              type="date"
              placeholder="Change end date"
              onChange={(event) => (this.ItemDetail.EndDate = event.currentTarget.value)}
              /> */}
          <button type="button" onClick={this.edit}>
            {' '}
            Click here to edit!
          </button>
        </div>
      </div>
    );
  }

  mounted() {
    todoService.getTodo(this.props.match.params.id, (Todo) => {
      this.ItemDetail = Todo;
      console.log(this.ItemDetail);
    });
  }

  edit() {
    todoService.updateTodo(this.ItemDetail, () => {
      console.log('You have now edited this todo item!');
      history.push('/todos');
    });
  }
}

ReactDOM.render(
  <HashRouter>
    <Route path="*s*" component={Menu} />{' '}
    {/* A simple hack to avoid showing the menu on the "login page".*/}
    <Route exact path="/" component={Home} />
    <Route exact path="/groups" component={GroupList} />
    <Route exact path="/groups/:id/lists" component={ListList} />
    <Route exact path="/Lists" component={ListList} />
    <Route exact path="/Lists/:id/todos" component={TodoList} />
    <Route exact path="/todos/:id" component={ToDoListDetails} />
  </HashRouter>,
  document.getElementById('root')
);
