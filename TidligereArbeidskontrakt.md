Arbeidskontrakt for bord 5 

Medlemmer: Fredrik Andresen, Rami Chumber, Emilie Bjerkås og Vegard Eidsmo 

Mål 

 

Effektmål 

Øke engasjementet for studiet, utvikle et bedre gruppemiljø i teamet og skape et trivelig arbeidsmiljø innad i teamet 

For å oppnå dette vil vi: 

Ha felles gruppeaktiviteter som er beregnet for å bli bedre kjent, slik at tillit økes. 

Teamet deltar på studiets arrangementer så godt det lar seg gjøre, for å øke motivasjon for studiet.  
 
 

Opparbeide gode arbeidsrutiner 

For å oppnå dette vil vi: 

Begrense bruk av mobil og pc til underholdning 

Sette opp klare arbeidsoppgaver mellom møtene 

Unngå kritikk av andres arbeid hvor hovedformål ikke er konstruktiv. 

Ha korte pauser når det er nødvendig. 

Opprettholde god kommunikasjon slik at hele teamet har en oversikt over den generelle fremdrift 

Spørre om noen trenger hjelp om man er ferdig med sin egen arbeidsoppgave.  
  

Være løsningsorienterte og fleksible i arbeidet. 

For å oppnå dette vil vi: 

Fokusere på mulige løsninger, fremfor å lete etter flere hinder 

Ha en positiv innstilling til- og hjelpe de andre teammedlemmene med å videreføre nye ideer. 

Spørre hverandre om hjelp om man trenger det, samtidig som man er åpen for å kunne hjelpe de ander i teamet. 

Rullere roller slik at hele teamet får erfaring og en bedre forståelse for hele prosessen i teamarbeidet. 

  

Resultatmål 

Være tidlig ute med levering av oppgaver. 

For å oppnå dette vil vi: 

Ferdigstille oppgaver senest 2 dager før innleveringsfristen for å gi nok tid til kvalitetssikring 
 
 

Bestå med god margin 

For å oppnå dette vil vi: 

Sørge for kvalitetssikring av alt arbeid før det leveres inn 

Holde frister 

Jobbe godt med arbeidsoppgavene før møtene slik at møtene er produktive. 
 
 

Fullføre studiet 

For å oppnå dette vil vi: 

Danne et godt kunnskapsgrunnlag for videre emner 

Sammen opprette og vedlikeholde gode rutiner for arbeid og møter 

  

Roller og oppgavefordeling 

 

For å sikre stabilitet og handlekraftighet skal teamet ha én fast teamleder. Denne teamlederen blir Fredrik. Da samtlige medlemmer skåret lavt på koordinator-rollen, ble vi enige om en leder som følte seg komfortabel med å holde oversikt over og å koordinere teamarbeidet.  

Ettersom Belbintesten viste at gruppa har en tendens til å foretrekke de samme rollene og på samme måte være mindre komfortabel med de samme rollene, vil det bli rullert på de andre rollene i teamet. Disse rollene vil dermed rullere hver andre uke, slik at alle gruppemedlemmene får prøvd seg i hver rolle. På denne måten holder teamet seg fleksibelt og medlemmene får muligheten til å utfordre seg selv i roller de føler seg mindre komfortabel i. 

 Rollefordelingen for de øvrige rollene blir slik: 

Ukenummer 

Møteleder 

Dokumentansvarlig 

Referent 

Innleveringsansvarlig 


 

Teamledelse 

Teamledelse innebærer å til enhver tid ha oversikt over gruppen og hva som må gjøres. Lederen delegerer oppgaver, avklarer uenigheter og styrer teamet dag til dag. Lederen har ansvar for at frister overholdes og at oppgaver gjennomføres til avtalt tid. Teamlederen har ansvar for å følge opp fravær. 
 
 

Møteleder 

Møteorganisering innebærer planlegging og gjennomføring av teammøter. Det betyr at man har ansvar for å sende ut en innkalling som tilfredsstiller gruppens forventninger i god tid før møtet (minst 2 dager før), booke grupperom, lede møtet samt sørge for at de andre teammedlemmene vet at det er møte. 

  

Dokumentansvarlig 

Dokumentansvarlig har ansvar for å gjennomføre og følge opp prosedyrene for dokumenthåndtering. Dokumentansvarlig har ansvar for at filer og dokumenter lastes opp riktig, systematiseres og sikkerhetskopieres. Dokumentansvarlig skal til enhver tid sørge for å ha en lokal sikkerhetskopi av filene i skyen. 

 

Referent 

Referenten skriver referatet til hvert teammøte. Referenten noterer hva som blir sagt på møter, samt hva teamet blir enig om. Referenten er også medansvarlig for at dokumentene på BB er ryddige og plassert riktig.  
 
 

Innleveringsansvarlig, kvalitetssikring av det som leveres 

Innleveringsansvarlig har ansvaret for at prosedyrene for innlevering av gruppearbeid blir fulgt. Dette innebærer å ha oversikt over tidsfrister og hva som må leveres inn. Oppfølging av gruppearbeider gjøres i samarbeid med teamleder. Innleveringsansvarlig har ansvar for at alt arbeid kvalitetssikres før det leveres inn. 

  

Prosedyrer 

 

Møter 

Møteinnkalling: Fast møtetidspunkt er hver tirsdag kl. 12:15 på Gløshaugen. Møtetidspunktet kan endres med flertall. Møteinnkallinger skal sendes ut senest 2 dager før avtalt møte, dette er det møteleder som har ansvaret for. Innkallingen skal inneholde tid og sted for møtet samt saksliste. Innkallinger sendes ut på e-post og i teamets Messenger-gruppe. 

Avstemning: Ved alle avgjørelser skal man ha en avstemning i gruppen. Alle teammedlemmer skal få anledning til å stemme. Medlemmer som ikke er til stede på møtet der beslutningen skal tas, får anledning til å stemme via Messenger-gruppen. Frist for avstemning er 23:59 samme dag som møtet holdes. 

 

Varsling ved fravær eller andre hendelser 

Dersom man kommer for sent eller ikke kan møte varsler man til lederen. En varsling om fravær må komme senest 08:15 samme morgen som møtet avholdes. Ved en akutthendelse varsler man leder ved første anledning.    

Ved fravær fra et møte, er det obligatorisk å lese over referatet for å få en oversikt over hva som har blitt diskutert og bestemt. Ved ytterligere spørsmål eller usikkerhet skal man kontakte møtets referent. Er det referent som melder frafall skal det stemmes over hvem som blir referent på gjeldede møte.  
 
 

Dokumenthåndtering 

Alt av dokumenter og filer skal sendes til dokumentansvarlig. Dokumentansvarlig skal så sørge for at alt lastes opp i gruppen “1digsec-H2020 Bord 05” i Blackboard, samt sikkerhetskopiere til en delt mappe med samme navn i Onedrive. Dokumentene på Onedrive skal systematiseres med interne mapper. 
 
 

Innleveringer av gruppearbeider 

Alle innleveringer skal godkjennes av samtlige teammedlemmer før innlevering, så godt det lar seg gjøre. Det er innleveringsansvarlig som har ansvar for at hele teamet får mulighet til å kontrollere innleveringene. Innleveringer skal ferdigstilles senest 2 dager før fristen for å legge til rette for kvalitetssikring. 

  

  

Interaksjon 

 

Oppmøte og forberedelse 

Godtatt oppmøtetidspunkt er oppmøtetidspunktet som står i innkallingen. Skulle noen være forsinket er det viktig at de sender en melding om dette, slik at resten av laget er klar over dette og kan vurdere å utsette møtet til alle er til stede. Krav til forberedelser er å lese møteinnkallingen. 
  

Tilstedeværelse og engasjement  

For felles økter skal pc og mobil i utgangspunktet ikke brukes for underholdning, ettersom dette kan gå utover gruppas produktivitet. Hvis noen ønsker en pause, avtaler de med gruppa om å ta en felles pause før arbeidet fortsetter igjen til en avtalt tid.  

Under møter skal distraksjoner unngås. Mobiler skal være på lydløs og ligge ute av syne og pc skal være lukket, med mindre de skal brukes under møtet. 
 

Hvordan støtte hverandre 

Det er viktig at alle i gruppa sier ifra om det er noe de er misfornøyde med knyttet til arbeidet. Det er da opp til resten av gruppa å finne en løsning på dette. Man skal ha møtekritikk i slutten av alle teammøter. 

For å sørge for god trivsel i teamet skal det arrangeres sosiale aktiviteter etter innlevering av gruppeprosjekter. 
 

Uenighet, avtalebrudd 

Ved uenighet i gruppa skal løsningene diskuteres i fellesskap og deretter kan gruppa stemme på det. Ved like mange stemmer for begge løsninger, har teamleder siste ord. Dersom uenigheten gjelder teamleder, har møteleder siste ord. Ved vedvarende uenighet tar teamet kontakt med faglærer. 

Dersom en i gruppa gjentatte ganger bryter avtaler (leverer ikke i tide, kommer for sent etc.) skal de sammen med gruppeleder finner ut av hva som ikke fungerer. Hvis de for eksempel har for stor arbeidsmengde, kan dette avlastes hos andre i gruppa. Dersom dette ikke virker, tar man kontakt med faglærer.